package main

import (
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"log"
	"quint.as/awsutils"
	"quint.as/iptracker"
)

// Handler is your Lambda function handler
// It uses Amazon API Gateway request/responses provided by the aws-lambda-go/events package,
// However you could use other event sources (S3, Kinesis etc), or JSON-decoded primitive types such as 'string'.
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// stdout and stderr are sent to AWS CloudWatch Logs
	log.Printf("Debug: ++Handler()")
	defer log.Printf("Debug: --Handler()")

	log.Printf("Info: Processing Lambda request %s\n", request.RequestContext.RequestID)

	host := request.QueryStringParameters["h"]

	if host == "" {
		log.Printf("Error: host parameter not provided in request")
		return awsutils.GenerateProxyResponse("{\"error\": \"host parameter not provided\"}", 400)
	}

	ipData, err := iptracker.GetMostRecentDbRecordForHost(host)

	if err != nil {
		log.Printf("Error: failed to get most recent db record for host " + host + ": " + err.Error())
		return awsutils.GenerateProxyResponse("{\"error\": \"failed to get most recent db record for host "+host+"\"}", 500)
	}

	b, err := json.Marshal(ipData)
	if err != nil {
		log.Printf("Error: failed to marshal json: " + err.Error())
		return awsutils.GenerateProxyResponse("{\"error\": \"failed to marshal json\"}", 500)
	}

	return awsutils.GenerateProxyResponse(string(b), 200)
}

func main() {
	lambda.Start(Handler)
}
