package main

import (
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/djhworld/go-lambda-invoke/golambdainvoke"
)

func main() {
	params := make(map[string]string)
	params["h"] = "slacktop"

	response, err := golambdainvoke.Run(golambdainvoke.Input{
		Port: 8001,
		Payload: events.APIGatewayProxyRequest{
			QueryStringParameters: params,
			Body:                  "",
		},
	})

	if err != nil {
		fmt.Println("Error: " + err.Error())
	} else {
		fmt.Println(string(response))
	}
}
