all: build package

build: ipGetter.go
	GOOS=linux go build ipGetter.go

package: ipGetter
	zip ipGetter.zip ipGetter

test:
	@echo ""
	@echo "Saving lambda function output to test.log!"
	@echo ""
	_LAMBDA_SERVER_PORT=8001 AWS_REGION=us-east-2 go run ipGetter.go >>test.log 2>test.log &
	go run testClient/testClient.go
